
## Working on Feature
```
$ git flow feature start module1

/* Then made the change and commit, even push to the server.
After all done */

$ git flow feature finish module1
```
+++

## Start a Feature from Server
![Create Feature in Bitbucket](assets/CreateFeatureBranchfromBitbucket.png)

+++

## Git Flow Chart
![Finish Feature Module1](assets/FinishFeatMod1.png)

+++

## Parallel Developing
![Parallel Developing](assets/Parallel-Developing.png)