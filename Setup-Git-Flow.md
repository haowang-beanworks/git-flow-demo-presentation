
## Git Flow Setup

Homebrew
```
$ brew install git-flow
```

Macports
```
$ port install git-flow-avh
```

Linux
```
$ apt-get install git-flow
```
Windows (Cygwin)

```
$ wget -q -O - --no-check-certificate https://raw.github.com/petervanderdoes/gitflow-avh/develop/contrib/gitflow-installer.sh install stable | bash
```

+++
## Git Flow Setup 
```
$ git flow init

Default convention names of branches
* Branch name for production releases [master]
* Branch name for "next release" development [develop]
* Feature branches? [feature/]
* Release branches? [release/]
* Hotfix branches? [hotfix/]
* Support branches? [support/]
```

+++
## Setup on Bitbucket
![Branches Set up in Bitbucket](assets/BranchingModelInBitbucket.png)

+++

## Create Branch on Bitbucket
![Create Branch in Bitbucket](assets/CreateBranchinBitbucket.png)
