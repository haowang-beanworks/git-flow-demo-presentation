#! /bin/bash
FILES="./*"
for f in $FILES
do
    echo "$f => ../$f"
    convert -density 6400 $f "../$f.png"
done