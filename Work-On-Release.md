## Working on Release
```
$ git flow release start 0.2.0

/* Then made the change and commit, even push to the server.
After all done */

$ git flow release finish 0.2.0
$ git push --tags
```

+++

## Release 0.2.0 Chart
![Release](assets/Release-0.2.0.png)