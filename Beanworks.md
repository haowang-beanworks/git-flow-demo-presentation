### How are we doing at Beanworks

+++

| Project | Develop Branch | Production Branch(es) | Use Release Branch | Use Tags |
|---------|----------------|-----------------------|--------------------|----------|
| API / UI | master | beta, stable | No | No |
| BeanAuth BeanLogger Haraka SFTP Mobile | master | production | No | No |
| SyncTool | master | master | No | No |

+++

## Summary

@ul
- We don't use Release Branches
    - Except `beta` is a long standing release branch
- We don't tag releases (not anymore)
- `SyncTool` is using `master` for both development and production
@ulend


