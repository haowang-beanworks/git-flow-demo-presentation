## What is GitFlow
* Gitflow is really just an abstract idea of a Git workflow.
* It dictates what kind of branches to set up and how to merge them together.

* People has made toolings around it. (Will Demo Later)

+++

### Essentials Of Gitflow

* Two trunk branches: `develop` and `master`
![DevelopAndMasterBranches](assets/GitFlowIntro-DevelopAndMaster.svg.png)

+++

@snap[north-west]
#### 
#### Feature Branches
@snapend

@snap[north-east]
* Branch off from `develop`
* Merge into `develop`
@snapend

@snap[south span-80]

![FeatureBranches](assets/GitFlowIntro-FeatureBranches.svg.png)

@snapend

+++

@snap[north-west span-30]
#### 
#### Release Branches
@snapend

@snap[north-east span-80]
* Branch off from `develop`
* Merge into `master` and `develop`
@snapend

@snap[south span-80]

![ReleaseBranches](assets/GitFlowIntro-ReleaseBranches.svg.png)

@snapend

+++

@snap[north-west]
#### 
#### Hotfix Branches
@snapend

@snap[north-east]
* Branch off from `master`
* Merge into `master` and `develop`
@snapend

@snap[south span-75]

![HotfixBranches](assets/GitFlowIntro-HotfixBranches.svg.png)

@snapend