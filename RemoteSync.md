## RemoteSync 
@snap[text-08]
1. Created the feature branch `RemoteSync' from Develop
2. Added a new vs Project (BEAN-8885 from phase 1) 
3. Refactoring (BEAN-9365 from phase 2)
4. Divided to two paths: 
    1. Unit tests (BEAN-9365)
    2. Logging interceptor (BEAN-9366 )
5. Put codes of BEAN-9365 and BEAN-9366 together
6. Merge to the feature branch `Remote Sync`(Task branch to feature branch)
7. Merge to the `Develop` branch(Finish Feature)
@snapend

Note:
Simpify but complicated enough for the demo
+++

## Started RemoteSync
![First Commit](assets/RemoteSyncFirstCommit-8885.png)

Note:
Ticket branch 8885 and then merge back to the feature branch.
+++

## Finished RemoteSync
![RemoteSync](assets/RemoteSyncDone.png)