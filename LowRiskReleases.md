## Low Risk Releases

+++

### Question:
@ul
1. What is the difference between deployment and release?
1. Deployment !== Release
@ulend

+++

### Deployment

> Deployment is the installation of a specified version of software to a given environment.
> ...
> Specifically, a deployment may or may not be associated with a release of a feature to customers.

> -- <cite>The DevOps Handbook</cite>

+++

### Release

> Release is when we make a feature available to all our customers or a segment of customers.

> -- <cite>The DevOps Handbook</cite>

+++

@snap[north-west span-50]
### Analogy

During WW2
@snapend

@snap[sourth-west span-50]
@ul
- Deployment: Deploy Troops to the UK over a period of years
- Release: Invade Normandy on June 6, 1944
@ulend
@snapend

@snap[north-east span-50]
![Release](https://i.dailymail.co.uk/1s/2018/12/03/13/6939266-6454549-A_total_of_78_convoys_delivered_four_million_tons_of_vital_cargo-a-79_1543843610803.jpg)
@snapend

@snap[south-east span-50]
![Release](https://vignette.wikia.nocookie.net/savingprivateryan/images/8/8f/Omaha.jpg/revision/latest?cb=20160411220745)
@snapend

+++
### What helps us to achieve decoupling between deployment and release?
+++
### Feature Flags
+++
### Ideal Workflow

@ul
- Development: Features are covered by Feature Flags
- Code is continuously being deployed to production environment
- Product Owner has control over when to turn on a feature flag to release the feature
- Also Known As 'Dark Launch'
@ulend

+++
### Why is this low risk
@ul
- Decouples Deployments and Releases
    - Dev Team can deploy at their own pace
        - Hopefully at every commit ![pray](https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/samsung/220/person-with-folded-hands_emoji-modifier-fitzpatrick-type-4_1f64f-1f3fd_1f3fd.png)
    - Product Team can release at their own pace
- Rollback is done by simply remove feature flags
- Developers are less afraid to deploy code
- (Enabled by / Enables) Continuous Deployment
@ulend