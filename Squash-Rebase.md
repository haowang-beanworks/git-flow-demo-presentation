## Message from Git
@snap[text-07 text-left]
Do not rebase commits that exist outside your repository and people may have based work on them.

If you follow that guideline, you’ll be fine. If you don’t, people will hate you, and you’ll be scorned by friends and family.

When you rebase stuff, you’re @color[red](abandoning) existing commits and creating new ones that are similar but different. If you push commits somewhere and others pull them down and base work on them, and then you rewrite those commits with git rebase and push them up again, your collaborators will have to re-merge their work and things will get messy when you try to pull their work back into yours.

-- https://git-scm.com/book/en/v2/Git-Branching-Rebasing

@snapend

+++ 

## Try to Avoid Squash
1. Created feature `feature/squash`
2. Update source code "Added Line1"
3. Merge the change to `develop`
4. Update source code "Added Line2"
5. Squashed the changes "Line1" & "Line2"
6. When try to merge the change to `develop` again, there is a conflict.
+++

## Squash Chart
![Squash](assets/Squash.png)

+++

## Squash PR Conflict
![Squash-PR](assets/Squash-PR.png)

+++

## Try to Avoid Rebase
@snap[text-07]
1. Created feature `feature/rebase`
2. Update source code "Added Line1"
3. Created `another-task-base-on-rebase-branch` from `feature/rebase`
4. Continued on `another..branch` to "Added Line2"
5. Becuase the `develop` has been changed by `feature/squash`, `feature/rebase`need bring in the changes.
6. But instead of merging the `develop`, `feature/rebase` rebased from `develop` 
7. When `another..branch` finish its job, try to merge `feature/rebase` will cause a conflict
@snapend
+++

## Rebase Chart
![Rebase](assets/Rebase.png)

+++

## Rebase PR Conflict
![Rebase PR](assets/Rebase-PR.png)

+++ 

## Rebase vs. Merge
@snap[text-07 text-left]
One point of view on this is that your repository’s commit history is a @color[red](record of what actually happened). It’s a historical document, valuable in its own right, and shouldn’t be tampered with. From this angle, changing the commit history is almost blasphemous; you’re lying about what actually transpired. So what if there was a messy series of merge commits? That’s how it happened, and the repository should preserve that for posterity.

The opposing point of view is that the commit history is the @color[red](story of how your project was made). You wouldn’t publish the first draft of a book, and the manual for how to maintain your software deserves careful editing. This is the camp that uses tools like rebase and filter-branch to tell the story in the way that’s best for future readers.

In general the way to get the best of both worlds is to rebase @color[red](local changes) you’ve made but haven’t shared yet before you push them in order to clean up your story, but never rebase anything you’ve @color[red](pushed somewhere).

@snapend